#ifndef ADD_APPLICATION_DIALOG
#define ADD_APPLICATION_DIALOG

#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <gtk/gtkwindow.h>

gboolean show_add_application_dialog (GtkWindow *parent, const char *mime_type);



#endif
