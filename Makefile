CC=gcc
CFLAGS=`pkg-config gtk+-2.0 libglade-2.0 gnome-vfs-module-2.0 gnome-vfs-2.0 libgnomeui-2.0 --cflags` -g -Wall 
LIBS=`pkg-config gtk+-2.0 libglade-2.0 gnome-vfs-module-2.0 gnome-vfs-2.0 libgnomeui-2.0 --libs`
#CFLAGS=`gnome-config gnomeui --cflags` -g -Wall
#LIBS=`gnome-config gnomeui --libs`

#-L. -lfoo

OBJS=	\
	add-application-dialog.o 		\
	edit-application-handlers-dialog.o \
	edit-file-type-dialog.o		\
	gnome-file-type-properties.o		\
	utils.o			

all: gnome-file-type-properties

clean: 
	rm -f *o gnome-file-type-properties core

gnome-file-type-properties: $(OBJS) 
	gcc -o gnome-file-type-properties $(OBJS) $(LIBS)

