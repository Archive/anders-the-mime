#include "add-application-dialog.h"
#include <glade/glade.h>
#include <gtk/gtk.h>
#include <libgnomevfs/gnome-vfs-application-registry.h>
#include <string.h>

#define _(x) (x)

typedef struct {
	GtkWidget *dialog;
	GtkWidget *app_radio;
	GtkWidget *custom_radio;
	GtkWidget *app_option_menu;
	GtkWidget *custom_app_table;

	GtkWidget *custom_name;
	GtkWidget *custom_command;
	GtkWidget *ok_button;
	char *mime_type;
	const char *description;
	gboolean dirty;

	GtkWidget *multiple_files;
	GtkWidget *uris;
	GtkWidget *needs_shell;
	
} AddApplicationDialog;


static gboolean
should_ok_be_sensitive (AddApplicationDialog *dialog)
{
	const char *name, *command;

	name = gtk_entry_get_text (GTK_ENTRY (dialog->custom_name));
	command = gtk_entry_get_text (GTK_ENTRY (dialog->custom_command));
	
	return (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->app_radio)) ||
		(name != NULL && strlen (name) > 0 && command != NULL && strlen (command) > 0));
}

static void
app_radio_toggled (GtkWidget *radio, AddApplicationDialog *dialog)
{
	gboolean app_active;

	app_active = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (radio));

	gtk_widget_set_sensitive (dialog->app_option_menu, app_active);
	gtk_widget_set_sensitive (dialog->custom_app_table, !app_active);
	gtk_widget_set_sensitive (dialog->ok_button,
				  should_ok_be_sensitive (dialog));
	
}

static gint
sort_app_func (gconstpointer a, gconstpointer b)
{
	const GnomeVFSMimeApplication *app_a, *app_b;

	app_a = a;
	app_b = b;

	return g_strcasecmp (app_a->name, app_b->name);
}

static void
populate_application_menu (AddApplicationDialog *dialog)
{
	GList *apps, *p;
	GtkWidget *menu, *item;
	GList *sorted_list;
	
	apps = gnome_vfs_application_registry_get_applications (NULL);
	menu = gtk_menu_new ();
	sorted_list = NULL;
	
	for (p = apps; p; p = p->next) {
		GnomeVFSMimeApplication *app;
		
		app = gnome_vfs_mime_application_new_from_id (p->data);
		if (app != NULL) {
			char *program = g_find_program_in_path (app->command);
			if (program != NULL) {
				sorted_list = g_list_insert_sorted (sorted_list, app,
								    sort_app_func);
				g_free (program);
			}
		}
	}
	for (p = sorted_list; p; p = p->next) {
		GnomeVFSMimeApplication *app = p->data;

		item = gtk_menu_item_new_with_label (app->name);
		g_object_set_data_full (G_OBJECT (item), "app_id",
					g_strdup (app->id), g_free);
		gtk_widget_show (item);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	}
	gtk_option_menu_set_menu (GTK_OPTION_MENU (dialog->app_option_menu), menu);
	
	g_list_free (sorted_list);
	g_list_free (apps);
}

static void
custom_name_or_command_changed (GtkWidget *entry, AddApplicationDialog *dialog)
{
	gtk_widget_set_sensitive (dialog->ok_button,
				  should_ok_be_sensitive (dialog));
}

static gboolean
handle_existing_app_in_list (AddApplicationDialog *dialog, GnomeVFSMimeApplication *app)
{
	GList *apps;

	/* Check if the app is already in the list */
	apps = gnome_vfs_mime_get_all_applications (dialog->mime_type);
	if (gnome_vfs_mime_id_in_application_list (app->id, apps)) {
		GtkWidget *message_dialog;
		char *message;
		int response;

		gnome_vfs_mime_application_list_free (apps);

		if (dialog->description != NULL) {
			message = g_strdup_printf (_("The application \"%s\" is already included "
						     "in the list of handlers for \"%s\" files. "
						     "Would you like to edit the existing \"%s\" handler instead?"),
						   app->name, dialog->description, app->name);
		}
		else {
			message = g_strdup_printf (_("The application \"%s\" is already included "
						     "in the list of handlers for this file type. "
						     "Would you like to edit the exiting \"%s\" handler instead?"),
						   app->name, app->name);
		}
		
		message_dialog = gtk_message_dialog_new (GTK_WINDOW (dialog->dialog),
							 GTK_DIALOG_MODAL,
							 GTK_MESSAGE_QUESTION,
							 GTK_BUTTONS_YES_NO,
							 message);
		gtk_dialog_set_default_response (GTK_DIALOG (message_dialog),
						 GTK_RESPONSE_YES);
		g_free (message);
		response = gtk_dialog_run (GTK_DIALOG (message_dialog));
		gtk_widget_destroy (message_dialog);
		
		if (response == GTK_RESPONSE_YES) {
			/* FIXME: Open the "Edit application" dialog here. */
		}

		return TRUE;
	}
	gnome_vfs_mime_application_list_free (apps);

	return FALSE;
}

static gboolean
handle_existing_custom_app (AddApplicationDialog *dialog)
{
	GList *apps, *p;
	char *command_path;
	gint response;
	GtkWidget *message_dialog;
	
	command_path = g_find_program_in_path (gtk_entry_get_text (GTK_ENTRY (dialog->custom_command)));

	if (command_path == NULL) {

		message_dialog = gtk_message_dialog_new (GTK_WINDOW (dialog->dialog),
							 GTK_DIALOG_MODAL,
							 GTK_MESSAGE_ERROR,
							 GTK_BUTTONS_OK,
							 _("The command \"%s\" can not be found. "
							   "Please check your spelling. Also, note that "
							   "commands are case-sensitive."),
							 gtk_entry_get_text (GTK_ENTRY (dialog->custom_command)));;
		response = gtk_dialog_run (GTK_DIALOG (message_dialog));
		gtk_widget_destroy (message_dialog);
		return FALSE;
	}
	else {
		/* We check if the command is a known one */
		apps = gnome_vfs_application_registry_get_applications (NULL);
		
		for (p = apps; p; p = p->next) {
			GnomeVFSMimeApplication *app;
			char *path;
			app = gnome_vfs_mime_application_new_from_id (p->data);
			if (app == NULL)
				continue;
			
			path = g_find_program_in_path (app->command);
			if (path == NULL)
				continue;

			if (strcmp (command_path, path) == 0) {
				char *message;
				GList *short_list;
				
				/* Now try to see if the app is in the short list */
				short_list = gnome_vfs_mime_get_all_applications (dialog->mime_type);
				
				if (gnome_vfs_mime_id_in_application_list
				    (app->id, short_list)) {
					if (dialog->description != NULL) {
						message = g_strdup_printf (_("The application you're trying to add has exactly the same "
									     "command as the \"%s\" application. However, \"%s\" is already "
									     "a handler for \"%s\" files. Would you like to edit the existing "
									     "handler instead?"),
									   app->name, app->name, dialog->description);
					}
					else {
						message = g_strdup_printf (_("The application you're trying to add has exactly the same "
									     "command as the \"%s\" application. However, \"%s\" is already "
									     "a handler for this file type. Would you like to edit the existing "
									     "handler instead?"),
									   app->name, app->name);
					}
					
					message_dialog = gtk_message_dialog_new (GTK_WINDOW (dialog->dialog),
										 GTK_DIALOG_MODAL,
										 GTK_MESSAGE_QUESTION,
										 GTK_BUTTONS_YES_NO,
										 message);
					g_free (message);
					response = gtk_dialog_run (GTK_DIALOG (message_dialog));
					gtk_widget_destroy (message_dialog);
					return FALSE;
				}
				else {
					if (dialog->description != NULL) {
						message = g_strdup_printf (_("The application you're trying to add has exactly the same "
									     "command as the \"%s\" application. Would you like to use "
									     "the \"%s\" application as a handler for \"%s\" files instead?"),
									   app->name, app->name, dialog->description);
					}
					else {
						message = g_strdup_printf (_("The application you're trying to add has exactly the same "
									     "command as the \"%s\" application. Would you like to add "
									     "the \"%s\" application as a handler for this file type instead?"),
									   app->name, app->name);
					}
					
					message_dialog = gtk_message_dialog_new (GTK_WINDOW (dialog->dialog),
										 GTK_DIALOG_MODAL,
										 GTK_MESSAGE_QUESTION,
										 GTK_BUTTONS_YES_NO,
										 message);
					g_free (message);
					response = gtk_dialog_run (GTK_DIALOG (message_dialog));
					gtk_widget_destroy (message_dialog);

					return FALSE;
					
				}
				g_list_free (short_list);
			}
		}
		g_list_free (apps);
	}

	return TRUE;
}

static void
try_to_add_app (AddApplicationDialog *dialog)
{
	GnomeVFSMimeApplication *app;

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->app_radio))) {
		GtkWidget *menu, *item;
		char *app_id;

		menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (dialog->app_option_menu));
		item = gtk_menu_get_active (GTK_MENU (menu));
		app_id = g_object_get_data (G_OBJECT (item), "app_id");
		app = gnome_vfs_mime_application_new_from_id (app_id);
		
		if (!handle_existing_app_in_list (dialog, app)) {
			gnome_vfs_application_registry_add_mime_type (app->id, dialog->mime_type);
			gnome_vfs_application_registry_sync ();
			gtk_widget_destroy (dialog->dialog);
		}
		gnome_vfs_mime_application_free (app);
	}
	else {
		gboolean result;
		char *app_id;
		GnomeVFSMimeApplication tmp;
	
		result = handle_existing_custom_app (dialog);

		if (!result)
			return;

		while (1) {
			app_id = g_strdup_printf ("custom-%x", g_random_int ());

			app = gnome_vfs_mime_application_new_from_id (app_id);
			if (app) {
				gnome_vfs_mime_application_free (app);
				g_free (app_id);
			}
			else {
				break;
			}
		}
		tmp.id = app_id;
		tmp.name = (char *)gtk_entry_get_text (GTK_ENTRY (dialog->custom_name));
		tmp.command = (char *)gtk_entry_get_text (GTK_ENTRY (dialog->custom_command));
		tmp.can_open_multiple_files = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->multiple_files));
		tmp.supported_uri_schemes = NULL;
		tmp.expects_uris = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->uris)) ?
			GNOME_VFS_MIME_APPLICATION_ARGUMENT_TYPE_URIS : GNOME_VFS_MIME_APPLICATION_ARGUMENT_TYPE_PATHS;
		tmp.requires_terminal = gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->needs_shell));
		
		gnome_vfs_application_registry_save_mime_application (&tmp);
		gnome_vfs_application_registry_add_mime_type (app_id, dialog->mime_type);
		gnome_vfs_mime_add_application_to_short_list (dialog->mime_type, app_id);
		gnome_vfs_application_registry_sync ();
		g_free (app_id);
		gtk_widget_destroy (dialog->dialog);
	}
}

static gboolean
should_add_app (AddApplicationDialog *dialog)
{

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->app_radio))) {
	}
#if 0
		return TRUE;
		/* Let's add app as a handler for our mime type */
			
		}
		else {
			char *command_path;


				

#endif
}

static void
dialog_response_callback (GtkWidget *d, gint response, AddApplicationDialog *dialog)
{
	
	switch (response) {
	case GTK_RESPONSE_OK:
		try_to_add_app (dialog);
	}
}

gboolean
show_add_application_dialog (GtkWindow *parent, const char *mime_type)
{
	GladeXML *glade;
	AddApplicationDialog *dialog;
	char *title;
	gboolean retval;
	
	glade = glade_xml_new ("gnome-file-type-properties.glade", "add-application-dialog", NULL);
	dialog = g_new (AddApplicationDialog, 1);
	dialog->dialog = glade_xml_get_widget (glade, "add-application-dialog");
	dialog->mime_type = g_strdup (mime_type);
	dialog->description = gnome_vfs_mime_get_description (dialog->mime_type);
	
	if (dialog->description != NULL) {
		title = g_strdup_printf (_("Add Application Handler For \"%s\" Files"), dialog->description);
	}
	else {
		title = g_strdup ("Add Application Handler");
	}
	gtk_window_set_title (GTK_WINDOW (dialog->dialog), title);

	dialog->multiple_files = glade_xml_get_widget (glade, "check-multiple");
	dialog->uris = glade_xml_get_widget (glade, "check-uris");
	dialog->needs_shell = glade_xml_get_widget (glade, "check-shell");
	
	dialog->ok_button = glade_xml_get_widget (glade, "ok-button");
	dialog->app_radio = glade_xml_get_widget (glade, "radio-select-app");
	g_signal_connect (dialog->app_radio, "toggled",
			  G_CALLBACK (app_radio_toggled), dialog);
	dialog->custom_radio = glade_xml_get_widget (glade, "radio-custom-app");
	dialog->app_option_menu = glade_xml_get_widget (glade, "app-option-menu");
	dialog->custom_app_table = glade_xml_get_widget (glade, "custom-app-table");
	populate_application_menu (dialog);

	dialog->custom_name = glade_xml_get_widget (glade, "custom-name");
	g_signal_connect (dialog->custom_name, "changed",
			  G_CALLBACK (custom_name_or_command_changed), dialog);
	dialog->custom_command = glade_xml_get_widget (glade, "custom-command");
	g_signal_connect (dialog->custom_command, "changed",
			  G_CALLBACK (custom_name_or_command_changed), dialog);

	g_signal_connect (dialog->dialog, "response",
			  G_CALLBACK (dialog_response_callback), dialog);
	g_signal_connect (dialog->dialog, "destroy",
			  G_CALLBACK (gtk_main_quit), NULL);
	gtk_widget_show_all (dialog->dialog);
	gtk_main ();
	retval = dialog->dirty;
	g_object_unref (glade);
	g_free (dialog);

	return retval;
}
