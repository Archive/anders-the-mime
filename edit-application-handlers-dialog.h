#ifndef __EDIT_APPLICATION_HANDLERS_DIALOG_H__
#define __EDIT_APPLICATION_HANDLERS_DIALOG_H__

#include <gtk/gtkdialog.h>

typedef struct _EditApplicationHandlersRetval EditApplicationHandlersRetval;

struct _EditApplicationHandlersRetval
{
  gboolean edited;
  GList *applications_list;
  GList *applications_short_list;
};

EditApplicationHandlersRetval *show_edit_application_handlers_dialog (GtkWindow      *parent,
								      GtkWindowGroup *group,
								      GList          *applications_list,
								      GList          *applications_short_list,
								      const char     *mime_type,
								      const char     *description);



#endif /* __EDIT_FILE_TYPE_DIALOG_H__ */
