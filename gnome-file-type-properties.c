#include <gtk/gtk.h>
#include <glade/glade.h>
#include <libgnomevfs/gnome-vfs-init.h>
#include <libgnomevfs/gnome-vfs-mime.h>
#include <libgnomevfs/gnome-vfs-mime-monitor.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <libgnomevfs/gnome-vfs-mime-info.h>
#include <libgnomeui/gnome-ui-init.h>
#include "edit-file-type-dialog.h"
#include "edit-application-handlers-dialog.h"
#include <string.h>

#define _(x) (x)
#define VERSION "1.0"

enum
{
  COL_MIMETYPE,
  COL_DESCRIPTION,
  COL_EXTENSIONS,
  NUM_COLS
};

enum
{
  SORT_BY_DESC_AND_MIME = NUM_COLS + 1,
};

typedef struct
{
  GtkWidget *dialog;
  GtkWidget *tree_view;
  GtkListStore *list_store;
  GtkTreeModel *sort_model;
  GtkWidget *add_button;
  GtkWidget *edit_button;
  GtkWidget *remove_button;
  gchar *new_mime_type;
} FileTypeDialog;

static void
selection_changed_callback (GtkTreeSelection *selection, FileTypeDialog *dialog)
{
  if (!gtk_tree_selection_get_selected (selection, NULL, NULL))
    {
      gtk_widget_set_sensitive (dialog->edit_button, FALSE);
      gtk_widget_set_sensitive (dialog->remove_button, FALSE);
    }
  else
    {
      gtk_widget_set_sensitive (dialog->edit_button, TRUE);
      gtk_widget_set_sensitive (dialog->remove_button, TRUE);
    }
}

static void
show_edit_mime_type_dialog (FileTypeDialog *dialog, GtkTreeIter *iter)
{
}

static void
add_mime_type_callback (GtkWidget *button, FileTypeDialog *dialog)
{
  show_edit_file_type_dialog (GTK_WINDOW (dialog->dialog), NULL);
}

static void
edit_mime_type_callback (GtkWidget *button, FileTypeDialog *dialog)
{
  GtkTreeIter iter;
  gboolean selected;
  char *mime_type;

  selected = gtk_tree_selection_get_selected (gtk_tree_view_get_selection (GTK_TREE_VIEW (dialog->tree_view)),
					      NULL, &iter);
  g_assert (selected);

  gtk_tree_model_get (GTK_TREE_MODEL (dialog->sort_model), &iter,
		      COL_MIMETYPE, &mime_type,
		      -1);
  show_edit_file_type_dialog (GTK_WINDOW (dialog->dialog),
			      mime_type);
  g_free (mime_type);
}


static void
row_activated_callback (GtkTreeView *view, GtkTreePath *path, GtkTreeViewColumn *column, FileTypeDialog *dialog)
{
  GtkTreeIter iter;

  gtk_tree_model_get_iter (GTK_TREE_MODEL (dialog->list_store), &iter, path);

  show_edit_mime_type_dialog (dialog, &iter);
}

static void
remove_mime_type_callback (GtkWidget *button, FileTypeDialog *dialog)
{
  GtkTreeIter iter;
  gboolean selected;
  char *description;
  char *mime_type;
  char *message;
  GtkWidget *message_dialog;
  gint response;

  selected = gtk_tree_selection_get_selected (gtk_tree_view_get_selection (GTK_TREE_VIEW (dialog->tree_view)),
					      NULL, &iter);
  g_assert (selected);
  gtk_tree_model_get (GTK_TREE_MODEL (dialog->list_store), &iter,
		      COL_MIMETYPE, &mime_type,
		      COL_DESCRIPTION, &description,
		      -1);
  if (description != NULL)
    {
      message = g_strdup_printf (_("Are you sure you want to remove the \"%s\" file type?"), description);
    }
  else
    {
      message = g_strdup (_("Are you sure you want to remove this file type?"));
    }

  message_dialog = gtk_message_dialog_new (GTK_WINDOW (dialog->dialog),
					   GTK_DIALOG_MODAL,
					   GTK_MESSAGE_QUESTION,
					   GTK_BUTTONS_YES_NO,
					   message);
  gtk_dialog_set_default_response (GTK_DIALOG (message_dialog), GTK_RESPONSE_YES);
  response = gtk_dialog_run (GTK_DIALOG (message_dialog));
  gtk_widget_destroy (GTK_WIDGET (message_dialog));

  if (response == GTK_RESPONSE_YES)
    {
      gnome_vfs_mime_registered_mime_type_delete (mime_type);
      gtk_list_store_remove (dialog->list_store, &iter);
    }

  g_free (message);
  g_free (mime_type);
  g_free (description);

}

static void
populate_filetypes_treeview (gpointer data)
{
  GList *mime_types, *p;
  GtkTreeIter iter;
  FileTypeDialog *dialog = data;
  gboolean iters_left;

  mime_types = gnome_vfs_get_registered_mime_types ();

  p = mime_types;
  iters_left = gtk_tree_model_get_iter_first (GTK_TREE_MODEL (dialog->list_store), &iter);

  while (p || iters_left)
    {
      char *extensions;
      const char *mime_type, *description;

      if (! iters_left)
	{
	  /* Add the rest of the mime types */
	  while (p)
	    {
	      mime_type = p->data;
	      description = gnome_vfs_mime_get_description (mime_type);
	      extensions = gnome_vfs_mime_get_extensions_pretty_string (mime_type);
	      if (extensions == NULL)
		extensions = g_strdup _("(None)");
	      gtk_list_store_append (dialog->list_store, &iter);
	      gtk_list_store_set (dialog->list_store, &iter,
				  COL_MIMETYPE, mime_type,
				  COL_DESCRIPTION, description,
				  COL_EXTENSIONS, extensions,
				  -1);
	      g_free (extensions);
	      p = p->next;
	    }
	}
      else if (p == NULL)
	{
	  /* Clean out the rest of the items */
	  while (iters_left)
	    {
	      GtkTreeIter next_iter = iter;
	      iters_left = gtk_tree_model_iter_next (GTK_TREE_MODEL (dialog->list_store), &next_iter);
	      gtk_list_store_remove (dialog->list_store, &iter);
	      iter = next_iter;
	    }
	}
      else
	{
	  /* We compare the two mime_types */
	  gchar *list_mime_type;
	  gint comparison;

	  mime_type = p->data;
	  gtk_tree_model_get (GTK_TREE_MODEL (dialog->list_store), &iter,
			      COL_MIMETYPE, &list_mime_type,
			      -1);
	  g_assert (mime_type);
	  g_assert (list_mime_type);

	  comparison = strcmp (mime_type, list_mime_type);
	  if (comparison < 0)
	    {
	      GtkTreeIter new_iter;
	      description = gnome_vfs_mime_get_description (mime_type);
	      extensions = gnome_vfs_mime_get_extensions_pretty_string (mime_type);
	      if (extensions == NULL)
		extensions = g_strdup _("(None)");
	      gtk_list_store_insert_before (dialog->list_store, &new_iter, &iter);
	      gtk_list_store_set (dialog->list_store, &new_iter,
				  COL_MIMETYPE, mime_type,
				  COL_DESCRIPTION, description,
				  COL_EXTENSIONS, extensions,
				  -1);
	      g_free (extensions);
	      p = p->next;
	    }
	  else if (comparison > 0)
	    {
	      GtkTreeIter next_iter = iter;
	      iters_left = gtk_tree_model_iter_next (GTK_TREE_MODEL (dialog->list_store), &next_iter);
	      gtk_list_store_remove (dialog->list_store, &iter);
	      iter = next_iter;

	    }
	  else
	    {
	      gchar *list_description;
	      gchar *list_extensions;

	      /* They're the same.  We update, then skip them both. */
	      gtk_tree_model_get (GTK_TREE_MODEL (dialog->list_store),
				  &iter,
				  COL_DESCRIPTION, &list_description,
				  COL_EXTENSIONS, &list_extensions,
				  -1);
	      description = gnome_vfs_mime_get_description (mime_type);
	      extensions = gnome_vfs_mime_get_extensions_pretty_string (mime_type);
	      if (extensions == NULL)
		extensions = g_strdup _("(None)");

	      if (description == NULL)
		{
		  if (list_description != NULL)
		    gtk_list_store_set (dialog->list_store, &iter,
					COL_DESCRIPTION, description,
					-1);
		}
	      else
		{
		  if (list_description == NULL ||
		      strcmp (list_description, description)) {
		    gtk_list_store_set (dialog->list_store, &iter,
					COL_DESCRIPTION, description,
					-1);
		  }
		}

	      if (strcmp (extensions, list_extensions))
		{
		  gtk_list_store_set (dialog->list_store, &iter,
				      COL_EXTENSIONS, extensions,
				      -1);
		}
	      g_free (list_description);
	      g_free (list_extensions);
	      g_free (extensions);
	      p = p->next;
	      iters_left = gtk_tree_model_iter_next (GTK_TREE_MODEL (dialog->list_store), &iter);
	    }
	  g_free (list_mime_type);

	}
    }
}

static gboolean
update_filetypes_idle (gpointer data)
{
  populate_filetypes_treeview (data);

  return FALSE;
}

static void
revert_to_defaults_callback (GtkWidget *button, FileTypeDialog *dialog)
{
  GtkWidget *message_dialog;
  gint response;

  message_dialog = gtk_message_dialog_new (GTK_WINDOW (dialog->dialog),
					   GTK_DIALOG_MODAL,
					   GTK_MESSAGE_QUESTION,
					   GTK_BUTTONS_YES_NO,
					   _("Reverting to system settings will lose any changes\n"
					     "you have ever made to your File Types.\n"
					     "Revert anyway?"));
  response = gtk_dialog_run (GTK_DIALOG (message_dialog));
  gtk_widget_destroy (GTK_WIDGET (message_dialog));

  if (response == GTK_RESPONSE_YES)
    {
      gnome_vfs_mime_reset ();
      gnome_vfs_mime_info_reload ();
      gtk_list_store_clear (dialog->list_store);
      populate_filetypes_treeview (dialog);
    }
}

static void
dialog_response_callback (GtkWidget *widget, gint response, FileTypeDialog *dialog)
{
  if (response == GTK_RESPONSE_CLOSE ||
      response == GTK_RESPONSE_DELETE_EVENT)
    {
      gtk_main_quit ();
    }
}

static gint
desc_and_mime_sort_func (GtkTreeModel *tree_model,
			 GtkTreeIter  *a,
			 GtkTreeIter  *b,
			 gpointer      user_data)
{
  gchar *name_a;
  gchar *name_b;
  gint retval;

  gtk_tree_model_get (tree_model, a, COL_DESCRIPTION, &name_a, -1);
  if (name_a == NULL)
    gtk_tree_model_get (tree_model, a, COL_MIMETYPE, &name_a, -1);
  if (name_a == NULL)
    name_a = g_strdup ("");

  gtk_tree_model_get (tree_model, b, COL_DESCRIPTION, &name_b, -1);
  if (name_b == NULL)
    gtk_tree_model_get (tree_model, b, COL_MIMETYPE, &name_b, -1);
  if (name_b == NULL)
    name_b = g_strdup ("");

  retval = g_utf8_collate (name_a, name_b);

  g_free (name_a);
  g_free (name_b);

  return retval;
}

static void
description_name_cell_func (GtkTreeViewColumn *tree_column,
			    GtkCellRenderer   *cell,
			    GtkTreeModel      *tree_model,
			    GtkTreeIter       *iter,
			    gpointer           data)
{
  gchar *name;

  gtk_tree_model_get (tree_model, iter, COL_DESCRIPTION, &name, -1);
  if (name == NULL)
    gtk_tree_model_get (tree_model, iter, COL_MIMETYPE, &name, -1);
  g_object_set (cell, "text", name, NULL);
  g_free (name);
}

static void
data_changed_callback (GObject *monitor,
		       gpointer data)
{
  g_idle_add_full (G_PRIORITY_LOW, update_filetypes_idle, data, NULL);
}

static void
setup_and_show_dialog (void)
{
  GnomeVFSMIMEMonitor *monitor;
  GladeXML *glade;
  GtkTreeViewColumn *column;
  GtkWidget *use_defaults_button;
  GtkCellRenderer *cell;
  FileTypeDialog *dialog;

  dialog = g_new (FileTypeDialog, 1);

  glade = glade_xml_new ("gnome-file-type-properties.glade", "file-type-dialog", NULL);

  dialog->dialog = glade_xml_get_widget (glade, "file-type-dialog");
  g_signal_connect (dialog->dialog, "response",
		    G_CALLBACK (dialog_response_callback), dialog);
  dialog->add_button = glade_xml_get_widget (glade, "add-file-type-button");
  g_signal_connect (dialog->add_button, "clicked",
		    G_CALLBACK (add_mime_type_callback), dialog);
  dialog->edit_button = glade_xml_get_widget (glade, "edit-file-type-button");
  g_signal_connect (dialog->edit_button, "clicked",
		    G_CALLBACK (edit_mime_type_callback), dialog);
  dialog->remove_button = glade_xml_get_widget (glade, "remove-file-type-button");
  g_signal_connect (dialog->remove_button, "clicked",
		    G_CALLBACK (remove_mime_type_callback), dialog);

  gtk_window_set_default_size (GTK_WINDOW (dialog->dialog), 500, 350);
  dialog->tree_view = glade_xml_get_widget (glade, "file-type-treeview");
  g_signal_connect (gtk_tree_view_get_selection (GTK_TREE_VIEW (dialog->tree_view)),
		    "changed", G_CALLBACK (selection_changed_callback), dialog);
  g_signal_connect (dialog->tree_view, "row_activated",
		    G_CALLBACK (row_activated_callback), dialog);
  dialog->list_store = gtk_list_store_new (NUM_COLS, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);
  dialog->sort_model = gtk_tree_model_sort_new_with_model (GTK_TREE_MODEL (dialog->list_store));
  gtk_tree_view_set_model (GTK_TREE_VIEW (dialog->tree_view), GTK_TREE_MODEL (dialog->sort_model));
  gtk_tree_sortable_set_default_sort_func (GTK_TREE_SORTABLE (dialog->sort_model), NULL, NULL, NULL);
  gtk_tree_view_set_search_column (GTK_TREE_VIEW (dialog->tree_view), COL_DESCRIPTION);
  cell = gtk_cell_renderer_text_new ();
  column = gtk_tree_view_column_new ();
  gtk_tree_view_column_set_title (column, _("Description"));
  gtk_tree_view_column_pack_start (column, cell, TRUE);
  gtk_tree_view_column_set_cell_data_func (column,
					   cell,
					   description_name_cell_func,
					   NULL, NULL);
  gtk_tree_sortable_set_sort_func (GTK_TREE_SORTABLE (dialog->sort_model),
				   SORT_BY_DESC_AND_MIME,
				   desc_and_mime_sort_func, NULL, NULL);
  gtk_tree_sortable_set_sort_column_id (GTK_TREE_SORTABLE (dialog->sort_model),
					SORT_BY_DESC_AND_MIME,
					GTK_SORT_ASCENDING);

  gtk_tree_view_column_set_sort_column_id (column, SORT_BY_DESC_AND_MIME);
  gtk_tree_view_append_column (GTK_TREE_VIEW (dialog->tree_view), column);

  column = gtk_tree_view_column_new_with_attributes (_("Extensions"),
						     gtk_cell_renderer_text_new (),
						     "text", COL_EXTENSIONS,
						     NULL);
  gtk_tree_view_column_set_sort_column_id (column, COL_EXTENSIONS);
  gtk_tree_view_append_column (GTK_TREE_VIEW (dialog->tree_view), column);

  use_defaults_button = glade_xml_get_widget (glade, "use-defaults-button");
  g_signal_connect (use_defaults_button, "clicked",
		    G_CALLBACK (revert_to_defaults_callback), dialog);
  gtk_widget_show_all (dialog->dialog);

  /* Add the mime types in an idle */
  g_idle_add_full (G_PRIORITY_LOW, update_filetypes_idle, dialog, NULL);

  monitor = gnome_vfs_mime_monitor_get ();
  g_signal_connect (G_OBJECT (monitor), "data_changed", G_CALLBACK (data_changed_callback), dialog);
}

gint
main (gint argc, gchar **argv)
{
  gnome_program_init ("gnome-file-type-properties", VERSION,
		      LIBGNOMEUI_MODULE, argc, argv, NULL);

  setup_and_show_dialog ();

  gtk_main ();

  return 0;
}
