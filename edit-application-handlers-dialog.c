#include "edit-application-handlers-dialog.h"
#include <gtk/gtk.h>
#include <glade/glade.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <libgnomevfs/gnome-vfs-application-registry.h>
#include "add-application-dialog.h"

#define _(x) (x)

typedef struct
{
  GtkWidget *dialog;
  char *mime_type;
  char *description;
  GtkListStore *list_store;
  GtkWidget *tree_view;

  GtkWidget *add_button;
  GtkWidget *edit_button;
  GtkWidget *remove_button;
  gboolean dirty;
  GList *applications_list;
  GList *applications_short_list;
} EditApplicationHandlersDialog;

enum
  {
    COL_NAME,
    COL_ID,
    COL_ENABLED,
    COL_USER_OWNED,
    COL_APPLICATION,
    NUM_COLS
  };

static void
populate_application_list (EditApplicationHandlersDialog *dialog)
{
  GList *p;

  gtk_list_store_clear (dialog->list_store);

  for (p = dialog->applications_list; p; p = p->next)
    {
      GnomeVFSMimeApplication *app = p->data;
      GtkTreeIter iter;
      gboolean in_short, user_owned;

      in_short = gnome_vfs_mime_id_in_application_list (app->id, dialog->applications_short_list);

      user_owned = gnome_vfs_application_is_user_owned_application (app);

      gtk_list_store_append (dialog->list_store, &iter);
      gtk_list_store_set (dialog->list_store, &iter,
			  COL_NAME, app->name,
			  COL_ENABLED, in_short,
			  COL_ID, app->id,
			  COL_USER_OWNED, user_owned,
			  COL_APPLICATION, gnome_vfs_mime_application_copy (app),
			  -1);
    }
}

static GList *
get_list (EditApplicationHandlersDialog *dialog, gboolean short_list)
{
  GtkTreeIter iter;
  GList *application_list = NULL;
  GnomeVFSMimeApplication *application;
  gboolean valid;
  gboolean enabled;

  for (valid = gtk_tree_model_get_iter_first (GTK_TREE_MODEL (dialog->list_store), &iter);
       valid;
       valid = gtk_tree_model_iter_next (GTK_TREE_MODEL (dialog->list_store), &iter))
    {
      gtk_tree_model_get (GTK_TREE_MODEL (dialog->list_store), &iter,
			  COL_ENABLED, &enabled,
			  COL_APPLICATION, &application,
			  -1);

      if (short_list)
	{
	  if (enabled)
	    application_list = g_list_prepend (application_list, gnome_vfs_mime_application_copy (application));
	}
      else
	{
	  application_list = g_list_prepend (application_list, application);
	}
    }

  return application_list;
}

static void
toggle_cell_toggled (GtkCellRenderer *toggle, const char *path_str, EditApplicationHandlersDialog *dialog)
{
  GtkTreePath *path;
  GtkTreeIter iter;

  dialog->dirty = TRUE;
  path = gtk_tree_path_new_from_string (path_str);
  gtk_tree_model_get_iter (GTK_TREE_MODEL (dialog->list_store), &iter, path);
  gtk_list_store_set (dialog->list_store, &iter,
		      COL_ENABLED, !gtk_cell_renderer_toggle_get_active (GTK_CELL_RENDERER_TOGGLE (toggle)),
		      -1);
  gtk_tree_path_free (path);
}

static void
selection_changed_callback (GtkTreeSelection *selection, EditApplicationHandlersDialog *dialog)
{
  GtkTreeIter iter;
  gboolean user_owned;

  if (gtk_tree_selection_get_selected (selection, NULL, &iter))
    {
      gtk_widget_set_sensitive (dialog->edit_button, TRUE);
      gtk_tree_model_get (GTK_TREE_MODEL (dialog->list_store), &iter,
			  COL_USER_OWNED, &user_owned,
			  -1);
      gtk_widget_set_sensitive (dialog->remove_button, user_owned);
    }
  else
    {
      gtk_widget_set_sensitive (dialog->edit_button, FALSE);
      gtk_widget_set_sensitive (dialog->remove_button, FALSE);
    }
}

static void
add_application_callback (GtkWidget *button, EditApplicationHandlersDialog *dialog)
{
#if 0
  gboolean dirty;

  dirty = show_add_application_dialog (GTK_WINDOW (dialog->dialog), dialog->mime_type);
  if (dirty)
    {
      dialog->dirty = TRUE;
      populate_application_list (dialog);
    }
#endif
}

EditApplicationHandlersRetval *
show_edit_application_handlers_dialog (GtkWindow      *parent,
				       GtkWindowGroup *group,
				       GList          *applications_list,
				       GList          *applications_short_list,
				       const char     *mime_type,
				       const char     *description)
{
  GladeXML *glade;
  EditApplicationHandlersDialog *dialog;
  GtkCellRenderer *cell;
  GtkTreeViewColumn *column;
  char *title;
  EditApplicationHandlersRetval *retval;
  gint response;

  retval = g_new0 (EditApplicationHandlersRetval, 1);
  dialog = g_new (EditApplicationHandlersDialog, 1);

  retval->edited = FALSE;

  glade = glade_xml_new ("gnome-file-type-properties.glade", "edit-application-handlers-dialog", NULL);
  dialog->mime_type = g_strdup (mime_type);
  dialog->description = g_strdup (description);
  dialog->dialog = glade_xml_get_widget (glade, "edit-application-handlers-dialog");
  dialog->add_button = glade_xml_get_widget (glade, "add-application");
  dialog->dirty = FALSE;
  dialog->applications_list = applications_list;
  dialog->applications_short_list = applications_short_list;
  g_signal_connect (dialog->add_button, "clicked", G_CALLBACK (add_application_callback), dialog);

  dialog->edit_button = glade_xml_get_widget (glade, "edit-application");
  dialog->remove_button = glade_xml_get_widget (glade, "remove-application");

  /* Set the title */
  if (description == NULL || *description == '\000')
    title = g_strdup ("Application handlers");
  else
    title = g_strdup_printf (_("Application handlers for \"%s\" files"), description);
  gtk_window_set_title (GTK_WINDOW (dialog->dialog), title);
  g_free (title);
  gtk_window_group_add_window (group, GTK_WINDOW (dialog->dialog));
  gtk_window_set_transient_for (GTK_WINDOW (dialog->dialog), parent);
  gtk_window_set_destroy_with_parent (GTK_WINDOW (dialog->dialog), TRUE);
  gtk_window_set_modal (GTK_WINDOW (dialog->dialog), TRUE);
  
  /* Set up application list */
  dialog->tree_view = glade_xml_get_widget (glade, "treeview");
  g_signal_connect (gtk_tree_view_get_selection (GTK_TREE_VIEW (dialog->tree_view)),
		    "changed", G_CALLBACK (selection_changed_callback), dialog);
  dialog->list_store = gtk_list_store_new (NUM_COLS,
					   G_TYPE_STRING,
					   G_TYPE_STRING,
					   G_TYPE_BOOLEAN,
					   G_TYPE_BOOLEAN,
					   G_TYPE_POINTER);
  gtk_tree_view_set_model (GTK_TREE_VIEW (dialog->tree_view), GTK_TREE_MODEL (dialog->list_store));
  gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (dialog->tree_view), FALSE);

  column = gtk_tree_view_column_new ();
  cell = gtk_cell_renderer_toggle_new ();
  g_signal_connect (cell, "toggled", G_CALLBACK (toggle_cell_toggled), dialog);
  gtk_tree_view_column_pack_start (column, cell, FALSE);
  gtk_tree_view_column_set_attributes (column, cell,
				       "active", COL_ENABLED,
				       NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (dialog->tree_view), column);

  column = gtk_tree_view_column_new ();
  cell = gtk_cell_renderer_text_new ();
  gtk_tree_view_column_pack_start (column, cell, TRUE);
  gtk_tree_view_column_set_attributes (column, cell,
				       "text", COL_NAME,
				       NULL);
  gtk_tree_view_append_column (GTK_TREE_VIEW (dialog->tree_view), column);

  populate_application_list (dialog);

  gtk_widget_show_all (dialog->dialog);

  /* Run the dialog */
  response = gtk_dialog_run (GTK_DIALOG (dialog->dialog));

  /* Unpack it */
  retval->edited = dialog->dirty && (response == GTK_RESPONSE_OK);
  if (retval->edited)
    {
      retval->applications_list = get_list (dialog, FALSE);
      retval->applications_short_list = get_list (dialog, TRUE);
    }

  gtk_widget_destroy (dialog->dialog);
  g_free (dialog->mime_type);
  g_free (dialog->description);
  g_free (dialog);
  g_object_unref (glade);

  return retval;
}
