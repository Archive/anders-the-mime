#include "edit-file-type-dialog.h"

#include <string.h>
#include <glade/glade.h>
#include <gtk/gtk.h>
#include <libgnomevfs/gnome-vfs-mime.h>
#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <libgnomevfs/gnome-vfs-mime-info.h>
#include <libgnomeui/gnome-icon-entry.h>

#include "utils.h"
#include "edit-application-handlers-dialog.h"

#define _(x) (x)

typedef struct {
  GtkWindowGroup *group;
  GtkWidget *dialog;
  GtkWidget *desc_entry;
  GtkWidget *mimetype_entry;
  char *mime_type;

  /* Extensions */
  GtkWidget *add_extension_button;
  GtkWidget *remove_extension_button;
  GtkWidget *extension_entry;
  GtkWidget *extensions_tree_view;
  GtkListStore *extensions_list_store;

  /* Application handlers */
  GtkWidget *viewer_radio;
  GtkWidget *application_radio;
  GtkWidget *handlers_menu;

  gint viewer_menu_history;
  gint application_menu_history;
  gboolean updating_menu;

  GList *applications_list;
  GList *components_list;
  GList *applications_short_list;
  GList *components_short_list;
} EditFileTypeDialog;

static void
selection_changed_callback (GtkTreeSelection *selection, EditFileTypeDialog *dialog)
{
  if (!gtk_tree_selection_get_selected (selection, NULL, NULL))
    gtk_widget_set_sensitive (dialog->remove_extension_button, FALSE);
  else
    gtk_widget_set_sensitive (dialog->remove_extension_button, TRUE);
}

static void
extension_entry_changed (GtkWidget *entry, EditFileTypeDialog *dialog)
{
  const gchar *text;
  text = gtk_entry_get_text (GTK_ENTRY (entry));

  if (text == NULL || *text == '\000')
    gtk_widget_set_sensitive (dialog->add_extension_button, FALSE);
  else
    gtk_widget_set_sensitive (dialog->add_extension_button, TRUE);
}


static void
add_extension_button_clicked_callback (GtkWidget *button, EditFileTypeDialog *dialog)
{
  const char *new_extension, *tmp;
  char *extension;
  GtkTreeIter iter;
  GtkTreePath *path;

  new_extension = gtk_entry_get_text (GTK_ENTRY (dialog->extension_entry));

  /* Check that the extension is valid */
  for (tmp = new_extension; tmp && *tmp; tmp = g_utf8_next_char (tmp))
    {
      gunichar c = g_utf8_get_char (tmp);

      if (g_unichar_isspace (c))
	{
	  GtkWidget *message_dialog;

	  message_dialog = gtk_message_dialog_new (GTK_WINDOW (dialog->dialog),
						   GTK_DIALOG_MODAL,
						   GTK_MESSAGE_WARNING,
						   GTK_BUTTONS_OK,
						   _("Space characters are not valid in file extentions."));
	  gtk_dialog_run (GTK_DIALOG (message_dialog));
	  gtk_widget_destroy (message_dialog);

	  return;
	}
    }
  /* Now check if the extension is already in the list */
  if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL (dialog->extensions_list_store), &iter))
    {
      do
	{
	  gtk_tree_model_get (GTK_TREE_MODEL (dialog->extensions_list_store),
			      &iter,
			      0, &extension,
			      -1);

	  if (strcmp (extension, new_extension) == 0)
	    {
	      GtkWidget *message_dialog;

	      message_dialog = gtk_message_dialog_new (GTK_WINDOW (dialog->dialog),
						       GTK_DIALOG_MODAL,
						       GTK_MESSAGE_WARNING,
						       GTK_BUTTONS_OK,
						       _("The file extension \"%s\" is already in the list of "
							 "file extensions and cannot be added."),
						       extension);
	      gtk_dialog_run (GTK_DIALOG (message_dialog));
	      gtk_widget_destroy (message_dialog);
	      g_free (extension);
	      return;
	    }
	  g_free (extension);
	} while (gtk_tree_model_iter_next (GTK_TREE_MODEL (dialog->extensions_list_store),&iter));
    }

  /* Now append the iter to the list */
  gtk_list_store_append (dialog->extensions_list_store, &iter);
  gtk_list_store_set (dialog->extensions_list_store, &iter,
		      0, new_extension,
		      -1);
  gtk_tree_selection_select_iter (gtk_tree_view_get_selection (GTK_TREE_VIEW (dialog->extensions_tree_view)), &iter);
  path = gtk_tree_model_get_path (GTK_TREE_MODEL (dialog->extensions_list_store), &iter);
  gtk_tree_view_scroll_to_cell (GTK_TREE_VIEW (dialog->extensions_tree_view), path, NULL, TRUE, 0.0, 1.0);
  gtk_entry_set_text (GTK_ENTRY (dialog->extension_entry), "");
}

static void
remove_extension_button_clicked_callback (GtkWidget *button, EditFileTypeDialog *dialog)
{
  gboolean selected;
  GtkTreeIter iter;

  selected = gtk_tree_selection_get_selected (gtk_tree_view_get_selection (GTK_TREE_VIEW (dialog->extensions_tree_view)), NULL, &iter);
  g_assert (selected);

  gtk_list_store_remove (dialog->extensions_list_store, &iter);
}

static void
entry_activated_callback (GtkWidget *entry, EditFileTypeDialog *dialog)
{
  const gchar *text;
  text = gtk_entry_get_text (GTK_ENTRY (entry));

  if (text == NULL || *text == '\000')
    gtk_widget_activate (dialog->add_extension_button);
  else
    gtk_window_activate_default (GTK_WINDOW (dialog->dialog));
}

static void
populate_handler_list (EditFileTypeDialog *dialog, GnomeVFSMimeActionType action_type, gboolean set_history_to_default)
{
  GtkWidget *menu, *item;
  GList *handlers, *p;
  gint history, i;

  dialog->updating_menu = TRUE;
  menu = gtk_menu_new ();

  switch (action_type)
    {
    case GNOME_VFS_MIME_ACTION_TYPE_NONE:
    case GNOME_VFS_MIME_ACTION_TYPE_APPLICATION:
      {
	GnomeVFSMimeApplication *default_application;
	gboolean found_default = FALSE;

	history = dialog->application_menu_history;
	default_application = gnome_vfs_mime_get_default_application (dialog->mime_type);

	if (dialog->applications_short_list == NULL && default_application == NULL)
	  {
	    item = gtk_menu_item_new_with_label (_("(None)"));
	    history = 0;
	    gtk_widget_show (item);
	    gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	  }
	else
	  {
	    for (i = 0, p = dialog->applications_short_list; p; p = p->next, i++)
	      {
		GnomeVFSMimeApplication *application = p->data;

		if (strcmp (application->id, default_application->id) == 0)
		  {
		    found_default = TRUE;
		    if (set_history_to_default)
		      {
			history = i;
		      }
		  }

		item = gtk_menu_item_new_with_label (application->name);
		gtk_widget_show (item);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	      }

	    /* We need to add the default application if it wasn't in the list */
	    if (!found_default)
	      {
		item = gtk_menu_item_new_with_label (default_application->name);
		gtk_widget_show (item);
		gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
		if (set_history_to_default)
		  {
		    history = i;
		  }
	      }
	  }
	break;
      }
    case GNOME_VFS_MIME_ACTION_TYPE_COMPONENT:
      history = dialog->viewer_menu_history;
      handlers = gnome_vfs_mime_get_short_list_components (dialog->mime_type);

      for (p = handlers; p; p = p->next)
	{
	  Bonobo_ServerInfo *component = p->data;
	  char *name;

	  name = name_from_bonobo_server_info (component);
	  item = gtk_menu_item_new_with_label (name);
	  g_free (name);
	  gtk_widget_show (item);
	  gtk_menu_shell_append (GTK_MENU_SHELL (menu), item);
	}
      gnome_vfs_mime_component_list_free (handlers);
      break;
    }

  gtk_option_menu_set_menu (GTK_OPTION_MENU (dialog->handlers_menu), menu);
  dialog->updating_menu = FALSE;
  gtk_option_menu_set_history (GTK_OPTION_MENU (dialog->handlers_menu), history);
}

static gboolean
check_new_mime_type (EditFileTypeDialog *dialog)
{
  const gchar *new_mime_type = gtk_entry_get_text (GTK_ENTRY (dialog->desc_entry));
  if (new_mime_type == NULL || new_mime_type[0] == '\0')
    {
      return FALSE;
    }
  if (gnome_vfs_mime_type_is_known (new_mime_type))
    {
      return FALSE;
    }
  return TRUE;
}

static void
response_callback (GtkWidget *dialog_widget,
		   gint response_id,
		   EditFileTypeDialog *dialog)
{
  if (response_id == GTK_RESPONSE_HELP)
    {
      /* FIXME: add help */
      return;
    }
  if (response_id == GTK_RESPONSE_OK)
    {
      GtkTreeIter iter;
      GList *short_list, *list;
      gnome_vfs_mime_freeze ();
      if (dialog->mime_type == NULL &&
	  (! check_new_mime_type (dialog)))
	return;

      gnome_vfs_mime_set_description (dialog->mime_type, gtk_entry_get_text (GTK_ENTRY (dialog->desc_entry)));
      if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL (dialog->extensions_list_store), &iter))
	{
	  GString *extensions;
	  char *extension;

	  extensions = g_string_new ("");
	  do
	    {
	      gtk_tree_model_get (GTK_TREE_MODEL (dialog->extensions_list_store),
				  &iter,
				  0, &extension,
				  -1);
	      g_string_append (extensions, extension);
	      g_string_append_c (extensions, ' ');
	    }
	  while (gtk_tree_model_iter_next (GTK_TREE_MODEL (dialog->extensions_list_store), &iter));
	  
	  gnome_vfs_mime_set_extensions_list (dialog->mime_type, extensions->str);
	  g_string_free (extensions, TRUE);
	}

      short_list = NULL;
      for (list = dialog->applications_short_list; list; list = list->next)
	short_list = g_list_prepend (short_list, ((GnomeVFSMimeApplication *)list->data)->id);
      gnome_vfs_mime_set_short_list_applications (dialog->mime_type, short_list);
      g_list_free (short_list);

      gnome_vfs_mime_thaw ();
    }

  gtk_widget_destroy (dialog->dialog);
  gnome_vfs_mime_application_list_free (dialog->applications_list);
  gnome_vfs_mime_application_list_free (dialog->applications_short_list);
  g_free (dialog->mime_type);
  g_free (dialog);

}

static void
viewer_radio_toggled (GtkWidget *radio, EditFileTypeDialog *dialog)
{
  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->viewer_radio)))
    populate_handler_list (dialog, GNOME_VFS_MIME_ACTION_TYPE_COMPONENT, FALSE);
  else
    populate_handler_list (dialog, GNOME_VFS_MIME_ACTION_TYPE_APPLICATION, FALSE);
}

static void
handlers_option_menu_changed (GtkWidget *menu, EditFileTypeDialog *dialog)
{
  /* Don't save history when we're updating the menu */
  if (dialog->updating_menu)
    return;

  if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (dialog->viewer_radio)))
    dialog->viewer_menu_history = gtk_option_menu_get_history (GTK_OPTION_MENU (menu));
  else
    dialog->application_menu_history = gtk_option_menu_get_history (GTK_OPTION_MENU (menu));
}

static void
edit_list_button_clicked (GtkWidget *button, EditFileTypeDialog *dialog)
{
  EditApplicationHandlersRetval *result;
  const gchar *description;
  description = gtk_entry_get_text (GTK_ENTRY (dialog->desc_entry));
  result = show_edit_application_handlers_dialog (GTK_WINDOW (dialog->dialog),
						  dialog->group,
						  dialog->applications_list,
						  dialog->applications_short_list,
						  dialog->mime_type, description);
  if (result->edited)
    {
      gnome_vfs_mime_application_list_free (dialog->applications_list);
      gnome_vfs_mime_application_list_free (dialog->applications_short_list);
      dialog->applications_list = result->applications_list;
      dialog->applications_short_list = result->applications_short_list;
    }
  g_free (result);
}

static EditFileTypeDialog *
create_new_dialog (void)
{
  GladeXML *glade;
  EditFileTypeDialog *dialog;
  GtkWidget *edit_list_button;

  glade = glade_xml_new ("gnome-file-type-properties.glade", "edit-file-type-dialog", NULL);
  dialog = g_new (EditFileTypeDialog, 1);
  dialog->dialog = glade_xml_get_widget (glade, "edit-file-type-dialog");
  dialog->desc_entry = glade_xml_get_widget (glade, "file-type-description");
  dialog->mimetype_entry = glade_xml_get_widget (glade, "file-type-mimetype");

  dialog->group = gtk_window_group_new ();
  gtk_window_group_add_window (dialog->group, GTK_WINDOW (dialog->dialog));
  
  /* Hook up the "Edit list" button */
  edit_list_button = glade_xml_get_widget (glade, "edit-list-button");
  g_signal_connect (edit_list_button, "clicked", G_CALLBACK (edit_list_button_clicked), dialog);

  /* Setup file extensions */
  dialog->extension_entry = glade_xml_get_widget (glade, "extension-entry");
  g_signal_connect (dialog->extension_entry, "activate",
		    G_CALLBACK (entry_activated_callback), dialog);
  dialog->add_extension_button = glade_xml_get_widget (glade, "add-extension-button");
  g_signal_connect (dialog->add_extension_button, "clicked",
		    G_CALLBACK (add_extension_button_clicked_callback), dialog);
  dialog->remove_extension_button = glade_xml_get_widget (glade, "remove-extension-button");
  g_signal_connect (dialog->remove_extension_button, "clicked",
		    G_CALLBACK (remove_extension_button_clicked_callback), dialog);
  g_signal_connect (dialog->extension_entry, "changed",
		    G_CALLBACK (extension_entry_changed), dialog);
  dialog->extensions_list_store = gtk_list_store_new (1, G_TYPE_STRING);
  dialog->extensions_tree_view = glade_xml_get_widget (glade, "extensions-treeview");
  g_signal_connect (gtk_tree_view_get_selection (GTK_TREE_VIEW (dialog->extensions_tree_view)),
		    "changed", G_CALLBACK (selection_changed_callback), dialog);
  gtk_tree_view_set_model (GTK_TREE_VIEW (dialog->extensions_tree_view),
			   GTK_TREE_MODEL (dialog->extensions_list_store));
  gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (dialog->extensions_tree_view), FALSE);
  gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (dialog->extensions_tree_view),
					       -1, NULL, gtk_cell_renderer_text_new (),
					       "text", 0,
					       NULL);

  /* Setup application handlers */
  dialog->viewer_radio = glade_xml_get_widget (glade, "radio-use-viewer");
  dialog->application_radio = glade_xml_get_widget (glade, "radio-open-with-application");
  dialog->handlers_menu = glade_xml_get_widget (glade, "handlers-menu");
  g_signal_connect (dialog->handlers_menu, "changed",
		    G_CALLBACK (handlers_option_menu_changed), dialog);
  dialog->viewer_menu_history = 0;
  dialog->application_menu_history = 0;

  g_signal_connect (dialog->viewer_radio, "toggled",
		    G_CALLBACK (viewer_radio_toggled), dialog);

  g_signal_connect (dialog->dialog, "response", G_CALLBACK (response_callback), dialog);


  
  g_object_unref (glade);
  return dialog;
}

static void
hydrate_edit_dialog (EditFileTypeDialog *dialog,
		     GtkWindow          *parent,
		     const char         *mime_type)
{
  const gchar *description;

  if (parent)
    {
      gtk_window_set_transient_for (GTK_WINDOW (dialog->dialog), parent);
      gtk_window_set_destroy_with_parent (GTK_WINDOW (dialog->dialog), TRUE);
    }

  dialog->viewer_menu_history = 0;
  dialog->application_menu_history = 0;

  if (mime_type)
    {
      GList *extension_list, *p;
      GnomeVFSMimeActionType mime_action_type;

      dialog->mime_type = g_strdup (mime_type);
      description = gnome_vfs_mime_get_description (mime_type);

      extension_list = gnome_vfs_mime_get_extensions_list (mime_type);
      for (p = extension_list; p; p = p->next)
	{
	  GtkTreeIter iter;
	  const char *extension = p->data;

	  gtk_list_store_append (dialog->extensions_list_store, &iter);
	  gtk_list_store_set (dialog->extensions_list_store, &iter,
			      0, extension,
			      -1);
	}
      gnome_vfs_mime_extensions_list_free (extension_list);
      mime_action_type = gnome_vfs_mime_get_default_action_type (mime_type);
      switch (mime_action_type)
	{
	case GNOME_VFS_MIME_ACTION_TYPE_NONE:
	case GNOME_VFS_MIME_ACTION_TYPE_APPLICATION:
	  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->application_radio), TRUE);
	  break;
	case GNOME_VFS_MIME_ACTION_TYPE_COMPONENT:
	  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (dialog->viewer_radio), TRUE);
	  break;
	}
      dialog->applications_list = gnome_vfs_mime_get_all_applications (mime_type);
      dialog->applications_short_list = gnome_vfs_mime_get_short_list_applications (mime_type);
      dialog->components_list = gnome_vfs_mime_get_all_components (mime_type);
      dialog->components_short_list = gnome_vfs_mime_get_short_list_components (mime_type);

      populate_handler_list (dialog, mime_action_type, TRUE);

    }
  else
    {
      dialog->mime_type = NULL;
      description = "";
      gtk_list_store_clear (dialog->extensions_list_store);
    }

  gtk_entry_set_text (GTK_ENTRY (dialog->desc_entry), description);
  gtk_entry_set_text (GTK_ENTRY (dialog->mimetype_entry), mime_type ? mime_type: "");
}

/* If mime_type is NULL, then we are in 'add' mode.  Otherwise, we edit them.
 */
void
show_edit_file_type_dialog (GtkWindow  *parent,
			    const char *mime_type)
{
  EditFileTypeDialog *dialog;

  dialog = create_new_dialog ();
  hydrate_edit_dialog (dialog, parent, mime_type);

  gtk_widget_show_all (dialog->dialog);
}
