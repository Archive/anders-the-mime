#ifndef __EDIT_FILE_TYPE_DIALOG_H__
#define __EDIT_FILE_TYPE_DIALOG_H__

#include <gtk/gtkwindow.h>

void show_edit_file_type_dialog (GtkWindow *parent, const char *mime_type);


#endif /* __EDIT_FILE_TYPE_DIALOG_H__ */
